#! bash

VERSION=$1

$(aws ecr get-login --no-include-email --region ap-southeast-2)

docker build -t zip-kubernetes .
docker tag zip-kubernetes:latest 227536281815.dkr.ecr.ap-southeast-2.amazonaws.com/zip-kubernetes:$VERSION
docker tag zip-kubernetes:latest 227536281815.dkr.ecr.ap-southeast-2.amazonaws.com/zip-kubernetes:latest
docker push 227536281815.dkr.ecr.ap-southeast-2.amazonaws.com/zip-kubernetes:$VERSION
docker push 227536281815.dkr.ecr.ap-southeast-2.amazonaws.com/zip-kubernetes:latest